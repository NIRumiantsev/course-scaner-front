import { makeAutoObservable } from 'mobx';
import { UserDto } from "types";

class UserStore {
  authorized: boolean = true
  currentUser: UserDto | null = null

  constructor(){
    makeAutoObservable(this)
  }

  setAuthorized(newStatus: boolean) {
    this.authorized = newStatus
  }

  setCurrentUser(newUser: UserDto) {
    this.currentUser = newUser
  }
}

const userStore = new UserStore();

export { UserStore, userStore };