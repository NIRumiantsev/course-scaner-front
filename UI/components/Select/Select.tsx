import { useState } from 'react';
import { BasicOption } from 'types';

type SelectProps = {
  value: string,
  onChange: (name: any, value: any) => void,
  name: string,
  options: BasicOption[],
  type?: string,
  style?: string,
  placeholder?: string,
}

const Select = (props: SelectProps) => {
  const {
    value,
    onChange,
    name,
    options,
    type = 'text',
    style = 'primary',
    placeholder = '',
  } = props;

  const [isOpened, setIsOpened] = useState<boolean>(false);

  const currentOption = options.find((option: BasicOption) =>
    option.value === value
  );

  return (
    <div>
      <div>
        <span>
          {currentOption?.label || placeholder}
        </span>
        <i onClick={() => setIsOpened(true)}>
          V
        </i>
      </div>
      {
        isOpened && (
          <ul>
            {
              options.map((option: BasicOption) =>
                <li
                  className={''}
                  onClick={() => {
                    setIsOpened(false);
                    onChange(name, option.value);
                  }}
                >
                  {option.label}
                </li>
              )
            }
          </ul>
        )
      }
    </div>
  )
};

export { Select };