type InputProps = {
  value: string,
  onChange: (name: any, value: any) => void,
  name: string,
  type?: string,
  style?: string,
  placeholder?: string,
}

const Input = (props: InputProps) => {
  const {
    value,
    onChange,
    name,
    type = 'text',
    style = 'primary',
    placeholder = '',
  } = props;

  return (
    <input
      onChange={(e) => onChange(name, e.target.value)}
      type={type}
      value={value}
      placeholder={placeholder}
    />
  )
};

export { Input };