type ButtonProps = {
  text: string,
  type: string,
}

const Button = (props: ButtonProps) => {
  const {
    text,
    type,
  } = props;

  return (
    <button>
      {text}
    </button>
  )
}

export { Button }