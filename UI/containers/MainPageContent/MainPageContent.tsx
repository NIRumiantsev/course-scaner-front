import { observer } from 'mobx-react';
import { userStore } from 'stores/UserStore/UserStore';

const MainPageContent = observer(() => {
  return (
    <div>{userStore.authorized ? 'ok' : 'ne ok'}</div>
  )
});

export { MainPageContent };