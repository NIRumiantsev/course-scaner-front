import { useState, FormEvent } from 'react';
import { Button, Input, Select } from 'UI/components';
import { UserDto } from 'types';
import { serviceLocator } from 'services/ServiceLocator';

const defaultUser = {
  email: '',
  firstName: '',
  lastName: '',
  password: '',
  phone: '',
  role: '',
};

const roleOptions = [
  { label: 'Преподаватель', value: 'TUTOR' },
  { label: 'Студент', value: 'STUDENT'},
];

const RegistrationForm = () => {
  const [formState, setFormState] = useState<UserDto>(defaultUser);
  const [errorMessage, setErrorMessage] = useState<string>('');

  const onFormSubmit = async (e: FormEvent) => {
    e.preventDefault();
    if (checkValidation()) {
      await serviceLocator.userService.createNewUser(formState);
    } else {
      setErrorMessage('Нужно заполнить все поля');
    }
  };

  const checkValidation = () => Object.values(formState).every((value: any) => {
    return !!value
  })

  const handleStateChange = (fieldName: keyof UserDto, newValue: string) => {
    const newState = {...formState, [fieldName]: newValue};
    setFormState(newState);
  };

  return (
    <form onSubmit={onFormSubmit}>
      <Input
        value={formState.email}
        name="email"
        placeholder="E-mail"
        onChange={handleStateChange}
      />
      <Input
        value={formState.password}
        name="password"
        placeholder="Пароль"
        onChange={handleStateChange}
      />
      <Input
        value={formState.phone}
        name="phone"
        placeholder="Номер телефона"
        onChange={handleStateChange}
      />
      <Input
        value={formState.firstName}
        name="firstName"
        placeholder="Имя"
        onChange={handleStateChange}
      />
      <Input
        value={formState.lastName}
        name="lastName"
        placeholder="Фамилия"
        onChange={handleStateChange}
      />
      <Select
        value={formState.role}
        onChange={handleStateChange}
        name="role"
        placeholder="Ваша роль"
        options={roleOptions}
      />
      <Button
        text="Отправить"
        type="primal"
      />
    </form>
  )
};

export { RegistrationForm };
