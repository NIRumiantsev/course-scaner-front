import { MainPageContent } from 'UI/containers/MainPageContent/MainPageContent';
import { AuthForm } from 'UI/containers/AuthForm/AuthForm';
import { RegistrationForm } from 'UI/containers/RegistrationForm/RegistrationForm';

export {
  MainPageContent,
  AuthForm,
  RegistrationForm,
};