import { useState, FormEvent } from 'react';
import { Button, Input } from 'UI/components';
import { AuthDto } from 'types';
import { serviceLocator } from 'services/ServiceLocator';
import { checkAuthAndLoadUser } from 'utils';

const AuthForm = () => {
  const [formState, setFormState] = useState<AuthDto>({login: '', password: ''});

  const onFormSubmit = async (e: FormEvent) => {
    e.preventDefault();
    await serviceLocator.authService.createAuthorisation(formState);
    await checkAuthAndLoadUser();
  };

  const handleStateChange = (fieldName: 'login' | 'password', newValue: string) => {
    const newState = {...formState, [fieldName]: newValue};
    setFormState(newState);
  };

  return (
    <form onSubmit={onFormSubmit}>
      <Input
        value={formState.login}
        name="login"
        placeholder="Логин"
        onChange={handleStateChange}
      />
      <Input
        value={formState.password}
        name="password"
        placeholder="Пароль"
        onChange={handleStateChange}
      />
      <Button
        text="Отправить"
        type="primal"
      />
    </form>
  )
};

export { AuthForm };
