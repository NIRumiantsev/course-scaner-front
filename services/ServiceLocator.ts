import { AuthService } from 'services/AuthService/AuthService';
import { UserService } from 'services/UserService/UserService';
import { userStore } from 'stores';

class ServiceLocator {
  readonly authService: AuthService
  readonly userService: UserService

  constructor() {
    this.authService = new AuthService(this)
    this.userService = new UserService(this, userStore)
  }
}

const serviceLocator = new ServiceLocator();

export { serviceLocator, ServiceLocator };