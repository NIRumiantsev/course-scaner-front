const COURSE_SCANNER_HOST = process.env.NEXT_PUBLIC_HOST;

const USERS_URL = `${COURSE_SCANNER_HOST}/users`;
const GET_USER_URL = (userId: string) =>
  `${USERS_URL}/${userId}`;

export { USERS_URL, GET_USER_URL };