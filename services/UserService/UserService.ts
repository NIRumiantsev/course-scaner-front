import { ServiceLocator } from '../ServiceLocator';
import { BaseService } from 'services/BaseService';
import { UserStore } from 'stores';
import { USERS_URL, GET_USER_URL } from 'services/UserService/urls';
import { UserDto } from 'types/UserTypes/UserDto';

class UserService extends BaseService {
  private readonly store: UserStore

  constructor(rootService: ServiceLocator, store: UserStore) {
    super(rootService);
    this.store = store;
  }

  async loadCurrentUser(id: string) {
    await this.api.get(GET_USER_URL(id))
      .then((response: any) => this.store.setCurrentUser(response))
  }

  setAuthorisation(newStatus: boolean) {
    this.store.setAuthorized(newStatus);
  }

  async createNewUser(userData: UserDto) {
    await this.api.post(USERS_URL, userData);
  }
}

export { UserService };
