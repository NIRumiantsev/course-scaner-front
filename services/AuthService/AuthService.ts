import { ServiceLocator } from '../ServiceLocator';
import { BaseService } from 'services/BaseService';
import { AUTH_URL } from './urls';
import { AuthDto } from 'types';
import Cookie from 'js-cookie'

class AuthService extends BaseService {
  constructor(rootService: ServiceLocator) {
    super(rootService);
  }

  async checkAuthorisation() {
    return await this.api.get(AUTH_URL)
  }

  async createAuthorisation(authorisationData: AuthDto) {
    await this.api.post(AUTH_URL, authorisationData)
      .then((response: any) => {
        Cookie.set("accessToken", response?.accessToken);
      })
      .catch((err) => {
        return err;
      });
  }
}

export { AuthService };
