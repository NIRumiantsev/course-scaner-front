const COURSE_SCANNER_HOST = process.env.NEXT_PUBLIC_HOST;

const AUTH_URL = `${COURSE_SCANNER_HOST}/auth`;

export { AUTH_URL };