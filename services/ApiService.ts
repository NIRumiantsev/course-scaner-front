import axios, { AxiosResponse } from 'axios';
import Cookie from 'js-cookie';

class ApiService {
  constructor(
    private readonly apiService = axios.create({
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Cookie.get('accessToken') || ''}`,
      }
    })
  ) {
    this.apiService.interceptors.response.use(this.handleSuccess, this.handleError);
  }

  private handleSuccess(response: AxiosResponse) {
    // тут можем как нибудь обработать успешный запрос
    return response;
  }

  private handleError(error: any) {
    // тут можем глобально обрабатывать ошибки в запросе
    return Promise.reject(error)
  }

  async get<T>(url: string) {
    const response = await this.apiService.get<T>(url);
    return response.data;
  }

  async post<T>(url: string, data?: any) {
    const response = await this.apiService.post<T>(url, data || '');
    return response.data;
  }

  async put<T>(url: string, data?: any) {
    const response = await this.apiService.post<T>(url, data || '');
    return response.data;
  }

  async delete<T>(url: string) {
    const response = await this.apiService.delete<T>(url);
    return response.data;
  }
}

export { ApiService }
