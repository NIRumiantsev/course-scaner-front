import {serviceLocator} from "services/ServiceLocator";

const checkAuthAndLoadUser = async () => {
  await serviceLocator.authService.checkAuthorisation()
    .then((response: any) => {
      serviceLocator.userService.setAuthorisation(true);
      serviceLocator.userService.loadCurrentUser(response.id);
    })
    .catch(() => serviceLocator.userService.setAuthorisation(false));
};

export { checkAuthAndLoadUser };