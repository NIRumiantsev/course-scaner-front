export type UserDto = {
  email: string,
  firstName: string,
  lastName: string,
  password: string,
  phone: string
  role: string
  __v?: number
  _id?: string
}
