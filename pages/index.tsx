import Link from 'next/link';
import styles from '../styles/Home.module.css';
import Head from 'next/head';
import { MainPageContent } from 'UI/containers';

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Course scanner</title>
      </Head>
      Main page
      <Link href={"/list"}>List page</Link>
      <MainPageContent/>
    </div>
  );
}
