import React from 'react';
import { useRouter } from 'next/router';
import Head from "next/head";

const ListPage = () => {
  const router = useRouter();
  const routerSlug = router.query.slug;
  const [direction, subDirection] = Array.isArray(routerSlug) ? routerSlug : [];
  return (
    <div>
      <Head>
        <title>Course scanner | Список курсов</title>
      </Head>
      List page {direction} {subDirection}
    </div>
  )
};

export default ListPage;