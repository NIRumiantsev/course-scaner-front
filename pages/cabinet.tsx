import React from 'react';
import Head from 'next/head';
import { AuthForm, RegistrationForm } from 'UI/containers';


const CabinetPage = () => {
  return (
    <div>
      <Head>
        <title>Course scanner | Личный кабинет</title>
      </Head>
      <AuthForm/>
      <RegistrationForm/>
    </div>
  )
};

export default CabinetPage;