import { useEffect } from 'react';
import type { AppProps } from 'next/app'
import 'styles/globals.css'
import { checkAuthAndLoadUser } from 'utils';

function MyApp({ Component, pageProps }: AppProps) {
  useEffect(() => {
    const checkAuthorisation = async () => {
      await checkAuthAndLoadUser()
    };
    checkAuthorisation();
  }, []);

  return <Component {...pageProps} />
}
export default MyApp
