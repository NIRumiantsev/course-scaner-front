import React from 'react';
import { useRouter } from 'next/router';
import Head from "next/head";

const CoursePage = () => {
  const router = useRouter();
  return (
    <div>
      <Head>
        <title>Course scanner | Страница курса</title>
      </Head>
      Course page {router.query.id}
    </div>
  )
};

export default CoursePage;